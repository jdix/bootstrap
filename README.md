# bootstrap

This project is intended to contain a bash script which will install ansible and use it to configure a linux pc with some preferred customizations and software. It will ideally be portable between fedora or debian based systems and include some triggers to help adjust based on hardware power (esp. graphics and memory capabilities - don't need games on a weak pc).

To run as a one-liner, use wget or curl respectively:
* wget -q -O /dev/stdout https://gitlab.com/jdix/bootstrap/-/raw/master/bootstrap.sh | bash
* curl https://gitlab.com/jdix/bootstrap/-/raw/master/bootstrap.sh | bash

TODO:
* copy python script and add zsh alias function to run it that updates remote ansible secrets
* aws vault profile creation?
* wireguard profiles

```bash
# prints objects in the ansible-secrets bucket
ave up aws s3api list-objects --bucket uptime-pros-ansible-secrets | jq '.Contents[].Key'

# updates an object in the ansible-secrets bucket
ave up aws s3api put-object --bucket uptime-pros-ansible-secrets --body ~/.aws/config --key aws-config
```


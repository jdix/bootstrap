#!/usr/bin/bash

# 0. Cleanup previous install
sudo rm -rf /opt/discord
sudo rm /usr/local/bin/discord
rm ~/.local/share/applications/discord.desktop

# https://unix.stackexchange.com/a/622995
# 1. Get tarball using Discord's API
wget "https://discordapp.com/api/download/stable?platform=linux&format=tar.gz" -O ~/Downloads/discord.tar.gz

# 2. extract to /opt or your preferred directory
sudo tar -xvf ~/Downloads/discord.tar.gz -C /opt/

# 2.5. Use a preferred lower case directory
sudo mv /opt/Discord /opt/discord

# 3. Symlink the executable into PATH
sudo ln -s /opt/discord/Discord /usr/local/bin/discord

# 4. Update incorrect paths in the .desktop file... 
sudo sed -i 's+Icon=discord+Icon=/opt/discord/discord.png+g' /opt/discord/discord.desktop
sudo sed -i 's+Exec=/usr/share/discord/Discord+Exec=/usr/local/bin/discord+g' /opt/discord/discord.desktop

# ... and symlink to expose an icon through the Desktop Environment. (tested with GNOME)
ln -s /opt/discord/discord.desktop ~/.local/share/applications

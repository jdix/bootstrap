#!/bin/bash

mkdir -p /etc/udev/rules.d
echo 'ACTION=="change"\' > /etc/udev/rules.d/90-mx-master-3-on.rules
echo ', SUBSYSTEM=="power_supply"\' >> /etc/udev/rules.d/90-mx-master-3-on.rules
echo ', ENV{POWER_SUPPLY_MODEL_NAME}=="Wireless Mouse MX Master 3"\' >> /etc/udev/rules.d/90-mx-master-3-on.rules
echo ', ENV{POWER_SUPPLY_ONLINE}=="1"\' >> /etc/udev/rules.d/90-mx-master-3-on.rules
echo ', ENV{POWER_SUPPLY_STATUS}=="Discharging"\' >> /etc/udev/rules.d/90-mx-master-3-on.rules
echo ", RUN+=\"/usr/bin/su root -c 'ratbagctl Logitech\ MX\ Master\ 3 dpi set 4000'\"" >> /etc/udev/rules.d/90-mx-master-3-on.rules

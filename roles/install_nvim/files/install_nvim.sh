#!/bin/bash

git clone https://github.com/neovim/neovim
cd neovim
git fetch && git pull
git checkout stable

if [ "$1" == "debug" ]; then
  echo -e "debug install\n\n\n"
  make CMAKE_EXTRA_FLAGS="-DCMAKE_INSTALL_PREFIX=$HOME/.local"
else
  echo -e "release install\n\n\n"
  make CMAKE_BUILD_TYPE=Release CMAKE_EXTRA_FLAGS="-DCMAKE_BUILD_TYPE=RelWithDebInfo -DCMAKE_INSTALL_PREFIX=$HOME/.local"
fi

make install

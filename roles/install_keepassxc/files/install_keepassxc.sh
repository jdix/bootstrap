#!/bin/bash


echo "This scipt installs the latest keepassxc from source control:
  - using working directory as a workdir
  - idempotently
  - to ~/.local
"

# keepassx build docs: https://github.com/keepassxreboot/keepassxc/wiki/Building-KeePassXC
echo "cloning keepassxc to check latest ver"
git clone https://github.com/keepassxreboot/keepassxc.git --single-branch --branch develop &>/dev/null
cd keepassxc
# check for current installed version, exit if already present
# todo: don't think this handles tags with "-beta"
LATEST_KEEPASSXC_VER=$(git tag -l | grep -P '^\d+\.\d+\.\d+$' | tail -n 1)
git checkout ${LATEST_KEEPASSXC_VER}

mkdir build
cd build
echo "running make commands"
cmake -DWITH_XC_ALL=ON \
  -DCMAKE_BUILD_TYPE=Release \
  -DWITH_XC_AUTOTYPE=ON \
  -DWITH_XC_YUBIKEY=ON \
  -DWITH_XC_BROWSER=ON \
  -DWITH_XC_NETWORKING=ON \
  -DWITH_XC_SSHAGENT=ON \
  -DWITH_XC_FDOSECRETS=ON \
  -DWITH_XC_KEESHARE=ON \
  -DWITH_XC_ALL=ON \
  -DCMAKE_INSTALL_PREFIX=~/.local \
  -DWITH_TESTS=ON \
  -DWITH_GUI_TESTS=ON \
  ..
make -j8 &>/dev/null
make install


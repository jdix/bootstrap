#!/bin/bash
set -ex
GIT_WORKDIR="${HOME}/code"
REPO_CLONE_DIR="bootstrap-autoclone"
BECOME_PASSWORD_FILE=become-password.txt
EXTRA_VARS=(
    "install_1320c_printer_drivers=no"
    "clear_nvim_cache_and_config_dirs=no"
    "reinstall_nvim=no"
)
if [[ -f $BECOME_PASSWORD_FILE ]]; then
    BECOME_PASS=`cat ${BECOME_PASSWORD_FILE}`
    EXTRA_VARS+="ansible_become_pass=${BECOME_PASS}"
fi

touch ${HOME}/.bashrc
source ${HOME}/.bashrc

function join_by { 
    local d=${1-} f=${2-}
    if shift 2; then
        printf %s "$f" "${@/#/$d}"
    fi
}

EXTRA_VARS=$(join_by ' ' ${EXTRA_VARS[*]})
[[ ! -z $EXTRA_VARS ]] && EXTRA_VARS="--extra-vars \"${EXTRA_VARS}\""

if [[ $1 != 'debug' ]]; then
    mkdir -p ${GIT_WORKDIR} && cd ${GIT_WORKDIR}
    rm -rf ${GIT_WORKDIR}/${REPO_CLONE_DIR}
    if [ -f /etc/redhat-release ]; then
        echo 'os redhat'
        sudo yum update && sudo yum upgrade -y && sudo yum install -y ansible git
    elif [ -f /etc/SuSE-release ]; then
        echo 'os suse'
        echo 'os not supported' && exit 1
    elif [ -f /etc/mandrake-release ]; then
        echo 'os mandrake'
        echo 'os not supported' && exit 1
    elif [ -f /etc/manjaro-release ]; then
        echo 'os manjaro'
        sudo pacman -Syu --noconfirm git ansible
    elif [ -f /etc/arch-release ]; then
        echo 'os arch'
        sudo pacman -Syu --noconfirm git ansible wget
    elif [ -f /etc/debian_version ]; then
        echo 'os debian'
        sudo apt update && sudo apt upgrade -y && sudo apt install -y ansible git
    fi

    git clone https://gitlab.com/jdix/bootstrap.git ${REPO_CLONE_DIR} && cd ./${REPO_CLONE_DIR}

    if [ -f /etc/arch-release ]; then
        ./arch-start.sh
    fi
fi

if [[ -f $BECOME_PASSWORD_FILE ]]; then
    eval "ansible-playbook -vv ${EXTRA_VARS} workstation.yml"
else
    eval "ansible-playbook -vv -K ${EXTRA_VARS} workstation.yml"
fi

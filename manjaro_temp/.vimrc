" Begin system's /etc/vimrc, ignored by aliasing vim/vi to 'vim -u ~/.vimrc'

if v:lang =~ "utf8$" || v:lang =~ "UTF-8$"
   set fileencodings=ucs-bom,utf-8,latin1
endif

set nocompatible            " Use Vim defaults (much better!)
set bs=indent,eol,start         " allow backspacing over everything in insert mode
"set ai                 " always set autoindenting on
"set backup             " keep a backup file
set viminfo='20,\"50            " read/write a .viminfo file, don't store more
                    " than 50 lines of registers
set history=50              " keep 50 lines of command line history
set ruler               " show the cursor position all the time
set nu                  " line numbering
set foldmethod=indent           " enable folding
set foldlevel=99
syntax on               " Switch syntax highlighting on, when the terminal has colors
set hlsearch                " Also switch on highlighting the last used search pattern
nnoremap <esc> :noh<return><esc>    " Clear the last search highlighting in Vim
" filetype plugin on
 
" hotkeys key binds shortcuts
nnoremap <space> za         " folding with the spacebar
nnoremap <C-J> <C-W><C-J>       " split navigations in one key combo instead of needing C-W first
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
 
" PEP 8 stuff for python
" au BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/

au BufNewFile,BufRead *.py
    \ set tabstop=4
    \| set softtabstop=4
    \| set shiftwidth=4
    \| set textwidth=79
    \| set expandtab
    \| set autoindent
    \| set fileformat=unix
    \| set encoding=utf-8

au BufNewFile,BufRead *.js,*.html,*.css
    \ set tabstop=2
    \| set softtabstop=2
    \| set shiftwidth=2

" Begin Vim Plug examples Plugins
call plug#begin('~/.vim/plugged')

Plug 'junegunn/vim-easy-align'
Plug 'SirVer/ultisnips' | Plug 'honza/vim-snippets'
Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }
Plug 'tpope/vim-fireplace', { 'for': 'clojure' }
Plug 'rdnetto/YCM-Generator', { 'branch': 'stable' }

" do i need go?
" Plug 'fatih/vim-go', { 'tag': '*' }
" Plug 'nsf/gocode', { 'tag': 'v.20150303', 'rtp': 'vim' }
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }

" example that seems to be for example purpose only
" Plug '~/my-prototype-plugin'

" Begin custom plugin additions
Plug 'vim-scripts/indentpython.vim'
Plug 'valloric/YouCompleteMe'
Plug 'kien/ctrlp.vim'
Plug 'nvie/vim-flake8'
Plug 'tpope/vim-fugitive'
Plug 'Lokaltog/powerline', { 'rtp': 'powerline/bindings/vim/' }

call plug#end()


" Plugin Config
let NERDTreeIgnore=['\.pyc$', '\~$'] " ignore files in nerdtree


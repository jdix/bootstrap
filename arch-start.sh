#!/bin/bash

# pending full automation of the below:
# - need to install yay by some other means
# - manjaro detection changed but not tested
# - perhaps change manjaro references to arch since it is based off arch not manjaro
# - had to remove wireguard confs that don't seem to be in s3 (file not found errors)
# - had to remove X11 display setting which was preventing sddm from loading
# - need to use dotnet install script?
#   - `dotnet-install.sh --channel 7.0`
#   - `~/.dotnet/dotnet tool install --global dotnet-ef --version 7.0.17`
# - install sqlcmd from https://github.com/microsoft/go-sqlcmd/releases/download/v1.6.0/sqlcmd-v1.6.0-linux-amd64.tar.bz2 to ~/.loacl/bin/
# - install discord script
# - add a hardware playbook option, consider such settings as:
# # /etc/environment
# STEAM_FORCE_DESKTOPUI_SCALING='1.5'
# ./roles/main/tasks/main.yml:#- name: graphics fix for amd w/ X11


# build package:
sudo pacman -Sy --needed git base-devel
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si

# binary package:
# pacman -S --needed git base-devel
# git clone https://aur.archlinux.org/yay-bin.git
# cd yay-bin
# makepkg -si

wget https://dot.net/v1/dotnet-install.sh -O dotnet-install.sh
chmod +x ./dotnet-install.sh
./dotnet-install.sh --version latest

~/.dotnet/dotnet tool install --global dotnet-ef
